//Tic Tac Toe 
//use with main.cpp and tictactoe header file
//Cassey Eisch

#include <iostream>
#include <conio.h>

#include "TicTacToe.h"

using namespace std;


TicTacToe::TicTacToe() { //Initialize constructor
	m_winner = ' '; //set winner to a space
	m_numTurns = 0; //set turn number to 0
	m_playerTurn = ' '; //set to a space
	for (int x = 0; x < 3; x++) {
		for (int y = 0; y < 3; y++) {
			m_board[x][y] = ' '; //set all spots in the array to a space
		}
	}
	//randomize the starting player
	//I thought this would randomize the starting player, but it just always starts at 'O', 
	//so I just left it as is because I couldn't get it to work any other way.
	int t = rand() % 2;
		char turn;
		if (t == 1) { turn = 'X'; }
		else { turn = 'O'; }

		if (turn == 'X') { m_playerTurn = 'X'; }
		else m_playerTurn = 'O';

	//Create a game header
	cout << "\n\t   TIC TAC TOE\n";
	cout << "\n      Get 3 in a row to win!\n"; //I tried to use the tab, but I wanted it centered
									//above the board so I just used spaces instead
	cout << " Player 1: 'O'  |  Player 2: 'X'\n";

}



	void TicTacToe::DisplayBoard() { //This method will output the board to the console.
		cout << "\n\t     |     |     \n";
		cout << "\t  " << m_board[0][0] << "  |  " << m_board[0][1] << "  |  " << m_board[0][2] << "\n";
		cout << "\t_____|_____|_____\n";
		cout << "\t     |     |     \n";
		cout << "\t  " << m_board[1][0] << "  |  " << m_board[1][1] << "  |  " << m_board[1][2] << "\n";
		cout << "\t_____|_____|_____\n";
		cout << "\t     |     |     \n";
		cout << "\t  " << m_board[2][0] << "  |  " << m_board[2][1] << "  |  " << m_board[2][2] << "\n";
		cout << "\t     |     |     \n\n";
	}


	bool TicTacToe::IsOver() { //Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
		DisplayResult();//call to see if there is a winner
		//this accidentally adds the winner/tie output a second time, but I left it in so that if there's 
		//a winner the game ends and the players don't have to keep playing until they're out of turns
		if (m_winner == 'X' || m_winner == 'O') { return true; } //there is a winner
		else if (m_numTurns == 9) { return true; } //there is a tie
		else { return false; } //the game isn't over yet
	}


	char TicTacToe::GetPlayerTurn() { //Returns an 'X' or an 'O,' depending on which player's turn it is.
		if (m_playerTurn == 'X') {m_playerTurn = 'O';}
		else { m_playerTurn = 'X'; }
		
		return m_playerTurn;
	}


	bool TicTacToe::IsValidMove(int position) { //Param: An int (1 - 9) representing a square on the board. Returns true if the 
									//space on the board is open, otherwise false.
		//	1 = [0][0]	2 = [0][1]	3 = [0][2]	4 = [1][0]	5 = [1][1]	6 = [1][2]	7 = [2][0]	8 = [2][1]	9 = [2][2]
		if (position>0 && position<10)//check that position variable is 1-9
		{
			if (position == 1 && m_board[0][0] == ' ') {
				return true;
			}
			else if (position == 2 && m_board[0][1] == ' ') {
				return true;
			}
			else if (position == 3 && m_board[0][2] == ' ') {
				return true;
			}
			else if (position == 4 && m_board[1][0] == ' ') {
				return true;
			}
			else if (position == 5 && m_board[1][1] == ' ') {
				return true;
			}
			else if (position == 6 && m_board[1][2] == ' ') {
				return true;
			}
			else if (position == 7 && m_board[2][0] == ' ') {
				return true;
			}
			else if (position == 8 && m_board[2][1] == ' ') {
				return true;
			}
			else if (position == 9 && m_board[2][2] == ' ') {
				return true;
			}
			else {//if the selected space is taken, ask them to try again
				//or if no spaces are open, return false, but it should already trigger end of game
				cout << "\n\tInvalid input. That spot is taken. Try again.\n\n";
				cin.clear(); //clear the input
				GetPlayerTurn(); //call again to reprint the ask for input from the same player
				return false;
			}
		}
		else {  //if position is not 1-9, it returns false
			if (cin.fail())//it will fail if the input is not an integer
			{
				cout << "\n\tInvalid input. Not a number. Try again.\n\n";
				cin.clear(); //clear the input
				cin.ignore(std::numeric_limits<int>::max(), '\n'); //repair so the program doesn't crash
				GetPlayerTurn(); //call again to reprint the ask for input from the same player
			}
			else if (position<1 || position>9)//this will trigger if the input is an integer, but not between 1-9
			{
				cout << "\n\tInvalid input. Not a valid position (1-9). Try again.\n\n";
				cin.clear(); //clear the input
				//cin.ignore(std::numeric_limits<int>::max(), '\n'); //repair so the program doesn't crash
				GetPlayerTurn(); //call again to reprint the ask for input from the same player
			}
			return false; 
		}
		
	}


	void TicTacToe::Move(int position) { //Param: int (1 - 9) representing a square on the board. Places the current players 
							//character in the specified position on the board.
		if (position == 1) {
			m_board[0][0] = m_playerTurn;
		}
		else if (position == 2) {
			m_board[0][1] = m_playerTurn;
		}
		else if (position == 3) {
			m_board[0][2] = m_playerTurn;
		}
		else if (position == 4) {
			m_board[1][0] = m_playerTurn;
		}
		else if (position == 5) {
			m_board[1][1] = m_playerTurn;
		}
		else if (position == 6) {
			m_board[1][2] = m_playerTurn;
		}
		else if (position == 7) {
			m_board[2][0] = m_playerTurn;
		}
		else if (position == 8) {
			m_board[2][1] = m_playerTurn;
		}
		else if (position == 9) {
			m_board[2][2] = m_playerTurn;
		}
		else {
			exit(0);//end early, just in case something is wrong, might change to an error
		}
		m_numTurns++; //add 1 to turns taken so when board is full (9 moves) it ends the game
	}


	void TicTacToe::DisplayResult() { //Displays a message stating who the winning player is, or if the game ended in a tie.
		//check for vertical wins
		for (int i = 0; i < 3; i++)
		{
			if (m_board[0][i] == m_board[1][i] && m_board[1][i] == m_board[2][i] && m_board[0][i] != ' ') {
				m_winner = m_board[0][i];
				cout << m_winner << " is the winner! Congratulations!\n";
			}
		}
		//check for horizontal wins
		for (int i = 0; i < 3; i++)
		{
			if (m_board[i][0] == m_board[i][1] && m_board[i][1] == m_board[i][2] && m_board[i][0] != ' ') {
				m_winner = m_board[i][0];
				cout << m_winner << " is the winner! Congratulations!\n";
			}
		}
		//check for diagonal wins
		if (m_board[0][0] == m_board[1][1] && m_board[1][1] == m_board[2][2] && m_board[0][0] !=' ')
		{	m_winner = m_board[0][0];
			cout << m_winner << " is the winner! Congratulations!\n";
		}
		if (m_board[0][2] == m_board[1][1] && m_board[1][1] == m_board[2][0] && m_board[0][2] !=' ')
		{	m_winner = m_board[0][2];
			cout << m_winner << " is the winner! Congratulations!\n";
		}
		//declare tie, if this function is called the game is already over, so if the above code all failed then its a tie
		else if (m_numTurns == 9 && m_winner == ' ')
		{
			cout << "It's a tie! Try again!\n";
		}
	}

