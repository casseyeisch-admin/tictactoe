//TicTacToe
//This header file is linked to the main.cpp to be used
//Cassey Eisch
 
#ifndef _TIC_TAC_TOE_H_
#define _TIC_TAC_TOE_H_

#include <iostream>
#include <conio.h>

using namespace std;


//create TicTacToe class
class TicTacToe 
{

private:
	char m_board[3][3]; //An array used to store the spaces of the game board. Once a player has Moved in one of the positions, 
						//the space should change to an X or an O.
	int m_numTurns = 0; //This will be used to detect when the board is full (all nine spaces are taken).
	char m_playerTurn; //This is used to track if it's X's turn or O's turn.
	char m_winner; //This is used to check to see the winner of the game. A space should be used while the game is being played.


public:
	TicTacToe(); //Constructor

	//other methods
	void DisplayBoard(); //This method will output the board to the console.

	bool IsOver(); //Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
		
	char GetPlayerTurn(); //Returns an 'X' or an 'O,' depending on which player's turn it is.

	bool IsValidMove(int position); //Param: An int (1 - 9) representing a square on the board. Returns true if the 
									//space on the board is open, otherwise false.

	void Move(int position); //Param: int (1 - 9) representing a square on the board. Places the current players 
							//character in the specified position on the board.

	void DisplayResult(); //Displays a message stating who the winning player is, or if the game ended in a tie.

};



#endif